<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: zcy
 * Date: 2023/1/8
 * Time: 9:36
 */

namespace General\Mq;


/**
 * 请求配置类
 * @method Worker run()
 */
class WorkerProperty
{
    /** @var int 内存（M） */
    private $memory = 128;

    /** @var int 等待时间（s） */
    private $sleep = 3;

    /** @var int 处理超时时间（s） */
    private $timeout = 60;

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     * @return $this
     */
    public function setTimeout(int $timeout): self
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @return int
     */
    public function getSleep(): int
    {
        return $this->sleep;
    }

    /**
     * @param int $sleep
     * @return $this
     */
    public function setSleep(int $sleep): self
    {
        $this->sleep = $sleep;
        return $this;
    }

    /**
     * @return int
     */
    public function getMemory(): int
    {
        return $this->memory;
    }

    /**
     * @param int $memoryLimit
     * @return $this
     */
    public function setMemory(int $memoryLimit): self
    {
        $this->memory = $memoryLimit;
        return $this;
    }
}
