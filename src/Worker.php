<?php

namespace General\Mq;


use General\Mq\Contract\BusinessHandle;
use General\Mq\Contract\Job;
use General\Mq\Contract\Queue;
use General\Mq\Event\Event;
use General\Mq\Event\EventMessage;
use General\Mq\Event\EventSet;
use General\Mq\Exceptions\AsyncTaskException;
use General\Mq\Exceptions\BusinessTaskException;
use General\Mq\Exceptions\NoMoreMessageException;
use General\Mq\Message\Message;
use General\Mq\Notify\DingNotify;
use Exception;
use Throwable;


/**
 * @property int $sleep
 * @property int $timeout
 * @property int $memory
 */
class Worker
{
    /** @var Queue */
    private $queueDriver;

    /** @var Event */
    private $event;

    /** @var WorkerProperty $workPropertyConfig */
    private $workPropertyConfig;

    /** @var string 队列名称 */
    private $queueName;

    /** @var $businessHandler BusinessHandle */
    private $businessHandler = null;

    /** @var bool $shouldQuit */
    private $shouldQuit;

    public function __construct(string $queueName, Event $event)
    {
        $this->event = $event;
        $this->queueName = $queueName;
        $this->bootstrap();
    }

    protected function bootstrap()
    {
        $this->workPropertyConfig = new WorkerProperty();
        $this->queueDriver = app('mq.connection');
    }

    protected function listenForEvents()
    {
        $this->event->attach(EventSet::FAILED,
            [new DingNotify(), 'notify'],
        );
    }

    /**
     * 设置业务处理器
     * @param BusinessHandle|null $handler
     * @return $this
     */
    public function setBusinessHandler(?BusinessHandle $handler = null): self
    {
        $this->businessHandler = $handler;
        return $this;
    }

    /**
     * 开始运行
     */
    public function run()
    {
        $this->registerSignalIfNecessary();
        while (true) {
            $this->checkWhetherToStop();

            $message = $this->getMessage();
            if (is_null($message)) {
                $this->sleep($this->sleep);
            }

            $payload = $message['payload'];
            if (empty($payload)) {
                continue;
            }

            if ($this->isSupportsAsyncSignals()) {
                $this->registerTimeoutHandler($message);
            }

            try {
                $messageEvent = $this->buildMessageEvent($message);
                // before event
                $this->event->trigger(EventSet::PROCESSING, $messageEvent);

                $this->confirm();
                if (!is_null($this->businessHandler)) {
                    $this->businessHandler($payload);
                } else {
                    $this->asyncTaskHandler($payload);
                }

                // after event
                $this->event->trigger(EventSet::PROCESSED, $messageEvent);
            } catch (BusinessTaskException $businessTaskException) {
                $this->failedMessageHandler($message, $businessTaskException);
            } catch (Throwable $throwable) {
                $this->event->trigger(EventSet::FAILED, $this->buildMessageEvent($payload, $throwable));
            }
        }
    }

    /**
     * 注册信号
     */
    protected function registerSignalIfNecessary()
    {
        if ($this->isSupportsAsyncSignals()) {
            $this->listenForSignals();
        }
    }

    /**
     * 监听事件
     * @param $event
     * @param callable ...$observer
     * @return $this
     */
    public function listenEvent($event, callable ...$observer): self
    {
        $this->event->attach($event, ...$observer);
        return $this;
    }

    /**
     * 是否支持异步
     * @return bool
     */
    protected function isSupportsAsyncSignals(): bool
    {
        return extension_loaded('pcntl');
    }

    /**
     * 监听信号
     */
    protected function listenForSignals()
    {
        pcntl_async_signals(true);
        pcntl_signal(SIGTERM, function () {
            $this->shouldQuit = true;
        });
    }

    /**
     * 检查停止情况
     */
    protected function checkWhetherToStop()
    {
        // 内存是否溢出
        if ($this->memoryExceeded($this->memory)) {
            $this->stop();
        }

        // 是否退出
        if ($this->shouldQuit) {
            $this->stop();
        }
    }

    /**
     * 内存判断
     * @param $memoryLimit
     * @return bool
     */
    protected function memoryExceeded($memoryLimit): bool
    {
        return (memory_get_usage(true) / 1024 / 1024) >= $memoryLimit;
    }

    /**
     * 退出
     * @param int $status
     */
    public function stop(int $status = 0)
    {
        exit($status);
    }

    /**
     * 获取消息
     * @return array|null
     */
    public function getMessage(): ?array
    {
        try {
            $message = $this->queueDriver->pull($this->queueName);
        } catch (NoMoreMessageException $noMoreMessageException) {
            return null;
        } catch (Exception $exception) {
            $this->event->trigger(EventSet::FAILED, $this->buildMessageEvent('not found', $exception));
            return null;
        }

        if (empty($message)) {
            return null;
        }
        $message = json_decode($message, true);
        $message = $this->buildMessageNecessary($message);
        $message['attempts']++;
        return $message;
    }

    /**
     * 如果有必要构建message
     * @param array $message
     * @return array
     */
    protected function buildMessageNecessary(array $message): array
    {
        $messageObject = new Message($message);
        $message = $messageObject->toArray();

        if (!isset($message['max_tries'])) {
            $message['max_tries'] = 3;
        }
        if (!isset($message['attempts'])) {
            $message['attempts'] = 0;
        }
        if (!isset($message['timeout'])) {
            $message['timeout'] = $this->timeout;
        }
        return $message;
    }

    /**
     * 暂停
     * @param $seconds
     */
    protected function sleep($seconds)
    {
        sleep($seconds);
    }

    /**
     * 注册超时handler
     * @param $message
     */
    protected function registerTimeoutHandler($message)
    {
        pcntl_signal(SIGALRM, function () use ($message) {
            if (extension_loaded('posix')) {
                posix_kill(getmypid(), SIGKILL);
            }
            $this->event->trigger(EventSet::FAILED,
                $this->buildMessageEvent($message, new Exception("执行超时")));
            exit(0);
        });
        $timeout = $message['timeout'] ?? $this->timeout;
        pcntl_alarm($timeout);
    }

    /**
     * @param $payload
     * @throws BusinessTaskException
     */
    public function businessHandler($payload)
    {
        try {
            $this->businessHandler->execute($payload);
        } catch (Exception $exception) {
            throw new BusinessTaskException(sprintf("消息消费执行失败 :%s", $exception->getTraceAsString()));
        }
    }

    /**
     * 确认消息
     */
    protected function confirm()
    {
        $this->queueDriver->ack();
    }

    /**
     * 异步任务
     * @param string $payload
     * @return mixed|void
     * @throws AsyncTaskException
     */
    public function asyncTaskHandler(string $payload)
    {
        try {
            $job = unserialize($payload);
            if ($job instanceof Job) {
                return $job->execute();
            }
        } catch (Exception $exception) {
            throw new AsyncTaskException(sprintf("异步任务执行失败 :%s", $exception->getTraceAsString()));
        }
    }

    /**
     * 错误消息处理
     * @param $message
     * @param $throwable
     */
    public function failedMessageHandler($message, $throwable)
    {
        // 重试
        if ((int)$message['attempts'] > (int)$message['max_tries']) {
            $this->event->trigger(EventSet::FAILED, $this->buildMessageEvent($message, $throwable));
            return;
        }
        $this->queueDriver->push($this->queueName, json_encode($message, JSON_UNESCAPED_UNICODE));
    }

    /**
     * @param $method
     * @param $params
     * @return mixed
     * @throws Exception
     */
    public function __call($method, $params)
    {
        try {
            return $this->workPropertyConfig->{$method}(...$params);
        } catch (Exception $e) {
            throw new Exception(sprintf("方法 %s不存在", $method));
        }
    }

    /**
     * @param $property
     * @return mixed
     * @throws Exception
     */
    public function __get($property)
    {
        try {
            $getter = sprintf('get%s', ucfirst($property));
            return $this->{$getter}();
        } catch (Exception $e) {
            throw new Exception(sprintf("属性 %s不存在", $property));
        }
    }

    /**
     * @param $property
     * @param $value
     * @return mixed
     * @throws Exception
     */
    public function __set($property, $value)
    {
        try {
            $setter = sprintf('set%s', ucfirst($property));
            return $this->{$setter}($value);
        } catch (Exception $e) {
            throw new Exception(sprintf("属性 %s不存在，无法赋值", $property));
        }
    }

    /**
     * @param $payload
     * @param null $exception
     * @return EventMessage
     */
    private function buildMessageEvent($payload, $exception = null): EventMessage
    {
        if (is_array($payload)) {
            $payload = json_encode($payload, JSON_UNESCAPED_UNICODE);
        }
        return new EventMessage(
            $this->queueName, $payload, $this->queueDriver->getName(), $exception
        );
    }
}
