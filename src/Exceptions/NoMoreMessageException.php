<?php

namespace General\Mq\Exceptions;

use Exception;

class NoMoreMessageException extends Exception
{
    public function __construct($message = null)
    {
        parent::__construct($message);
    }
}
