<?php

namespace General\Mq\Exceptions;

class AsyncTaskException extends \Exception
{
    public function __construct($message = null)
    {
        parent::__construct($message);
    }
}
