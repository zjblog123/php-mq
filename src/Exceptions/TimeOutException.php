<?php

namespace General\Mq\Exceptions;

class TimeOutException extends \Exception
{
    public function __construct($message = null)
    {
        parent::__construct($message);
    }
}
