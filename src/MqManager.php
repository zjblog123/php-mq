<?php

namespace General\Mq;

use General\Mq\Contract\Connector as ConnectorInterface;
use General\Mq\Contract\Queue as MqInterface;
use General\Mq\Resources\ConfigBean;
use Exception;
use InvalidArgumentException;
use Closure;

class MqManager
{
    /**
     * @var ConfigBean
     */
    protected $configBean;

    /**
     * mq驱动集合
     * @var array
     */
    protected $mqComponents = [];

    /**
     * 连接器集合
     * @var array
     */
    protected $connectors = [];

    public function __construct()
    {
        $this->configBean = new ConfigBean();
    }

    /**
     * 获取驱动
     * @param string|null $name
     * @return MqInterface
     */
    public function connection(string $name = null): MqInterface
    {
        $name = $name ?: $this->getDefaultDriver();
        if (!isset($this->mqComponents[$name])) {
            $this->mqComponents[$name] = $this->resolve($name);
        }
        return $this->mqComponents[$name];
    }

    /**
     * 获取驱动
     * @return string
     */
    public function getDefaultDriver(): string
    {
        return $this->configBean->getDefaultDriver();
    }

    /**
     * 解析
     * @param $name
     * @return MqInterface
     * @throws Exception
     */
    protected function resolve($name): MqInterface
    {
        $config = $this->getConfig($name);
        if (empty($config)) {
            throw new Exception("解析失败");
        }
        return $this->getConnector($config['driver'])->connect($config)->setName($name);
    }

    /**
     * 获取队列配置
     * @param string|null $name
     * @return array
     */
    protected function getConfig(string $name = null): array
    {
        if (!is_null($name)) {
            return $this->configBean->getConfig($name);
        }
        return [];
    }

    /**
     * 获取连接器
     * @param $driver
     * @return ConnectorInterface
     */
    protected function getConnector($driver): ConnectorInterface
    {
        if (!isset($this->connectors[$driver])) {
            throw new InvalidArgumentException("No connector for [$driver]");
        }
        return call_user_func($this->connectors[$driver]);
    }

    public function addConnector($driver, Closure $resolver)
    {
        $this->connectors[$driver] = $resolver;
    }

    public function extend($driver, Closure $resolver)
    {
        $this->addConnector($driver, $resolver);
    }
}
