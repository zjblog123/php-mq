<?php

namespace General\Mq\Event;

class Event
{
    public $observers = [];

    /**
     * 注册监听者
     * @param $event
     * @param callable ...$observer
     */
    public function attach($event, callable ...$observer)
    {
        $this->observers[$event] = array_merge($this->observers[$event] ?? [], $observer);
    }

    /**
     * 触发事件
     * @param $event
     * @param EventMessage|null $eventMessage
     */
    public function trigger($event, ?EventMessage $eventMessage)
    {
        /** @var callable $observer */
        foreach ($this->getListeners($event) as $observer) {
            call_user_func($observer, $eventMessage);
        }
    }

    /**
     * 获取监听者
     * @param $event
     * @return array
     */
    public function getListeners($event): array
    {
        return $this->observers[$event] ?? [];
    }
}
