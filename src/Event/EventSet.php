<?php


namespace General\Mq\Event;


class EventSet
{
    // 运行前
    const PROCESSING = 'processing';

    // 运行后
    const PROCESSED = 'processed';

    // 错误
    const FAILED = 'failed';
}
