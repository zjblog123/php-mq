<?php

namespace General\Mq\Event;

use \Throwable;

class EventMessage
{
    public $queueName;

    public $payload;

    public $exception;

    public $queueDriver;

    /**
     * @param $queueName
     * @param $payload
     * @param $queueDriver
     * @param Throwable|null $exception
     */
    public function __construct($queueName, $payload, $queueDriver, ?Throwable $exception = null)
    {
        $this->queueName = $queueName;
        $this->payload = $payload;
        $this->exception = $exception;
        $this->queueDriver = $queueDriver;
    }
}
