<?php

namespace General\Mq\Failed;

use General\Mq\Contract\Failed as FailedContract;
use General\Mq\Event\EventMessage;
use General\Mq\Resources\ConfigBean;
use General\Mq\Resources\DatabaseBean;
use Illuminate\Support\Facades\Date;
use Exception;

class DatabaseFailed implements FailedContract
{
    /**
     * @var DatabaseBean
     */
    protected $databaseBean;

    /**
     * @var ConfigBean
     */
    protected $configBean;

    /**
     * @var String
     */
    protected $failedTable;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->databaseBean = new DatabaseBean();
        $this->configBean = new ConfigBean();

        $this->failedTable = $this->configBean->getFailedTable();
        if (empty($this->failedTable)) {
            throw new Exception("mq配置缺少错误记录表名");
        }
    }

    public function log(EventMessage $eventMessage)
    {
        return $this->databaseBean->insert($this->failedTable, [
            $eventMessage->queueDriver,
            $eventMessage->queueName,
            json_encode($eventMessage->payload),
            (string) $eventMessage->exception,
            Date::now()
        ]);
    }

    /**
     * Get a list of all of the failed message.
     */
    public function all()
    {
        return $this->databaseBean->all($this->failedTable);
    }

    /**
     * Get a single failed message.
     *
     * @param int $id
     */
    public function find($id)
    {
        return $this->databaseBean->find($this->failedTable, $id);
    }

    /**
     * Delete a single failed message from storage.
     */
    public function forget($id)
    {
        return $this->databaseBean->delete($this->failedTable, $id);
    }

    /**
     * Flush all of the failed messages from storage
     *
     * @return void
     */
    public function flush()
    {
        $this->databaseBean->truncate($this->failedTable);
    }
}
