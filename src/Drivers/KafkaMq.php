<?php

namespace General\Mq\Drivers;

use General\Mq\Contract\Queue as MqInterface;
use General\Mq\Exceptions\NoMoreMessageException;
use General\Mq\Exceptions\TimeOutException;
use RdKafka\KafkaConsumer;
use RdKafka\Producer;

class KafkaMq extends BaseMq implements MqInterface
{
    protected $producer = null;

    protected $consumer = null;

    public function __construct(Producer $producer, KafkaConsumer $consumer)
    {
        $this->producer = $producer;

        $this->consumer = $consumer;
    }

    public function push(string $topic, string $message, array $headers = NULL, int $delay = 0)
    {
        $producerTopic = $this->producer->newTopic($topic);

        // RD_KAFKA_PARTITION_UA 表示自动分区
        $producerTopic->producev(RD_KAFKA_PARTITION_UA, 0, $message, NULL, $headers);

        // 轮询产生的事件
        // https://arnaud.le-blanc.net/php-rdkafka-doc/phpdoc/rdkafka.poll.html
        $this->producer->poll(0);

        for ($flushRetries = 0; $flushRetries < 3; $flushRetries++) {
            $result = $this->producer->flush(10000);
            if (RD_KAFKA_RESP_ERR_NO_ERROR === $result) {
                break;
            }
        }

        if (RD_KAFKA_RESP_ERR_NO_ERROR !== $result) {
            throw new \RuntimeException('Was unable to flush, messages might be lost! ' . $result);
        }
    }

    /**
     * @return String
     * @throw \Exception
     */
    public function pull(string $topic)
    {
        $this->consumer->subscribe((array) $topic);

        // return RdKafka\Message
        $rdKafkaMessage = $this->consumer->consume(120*1000);
        switch ($rdKafkaMessage->err) {
            case RD_KAFKA_RESP_ERR_NO_ERROR:
                return $rdKafkaMessage->payload;
                break;
            case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                throw new NoMoreMessageException("No more messages!");
                break;
            case RD_KAFKA_RESP_ERR__TIMED_OUT:
                throw new TimeOutException("Timed out!");
                break;
            default:
                throw new \Exception($rdKafkaMessage->errstr(), $rdKafkaMessage->err);
                break;
        }
    }

    /**
     * 清空消息
     * @param string $topic
     * @return mixed
     */
    public function flush(string $topic)
    {
        // todo
    }

    /**
     * 消息长度
     * @param int $topic
     * @return mixed
     */
    public function len(int $topic)
    {
        // todo
    }

    /**
     * 消费者 -- 消息确认
     */
    public function ack()
    {
        $this->consumer->commit();
    }

    /**
     * 提供原生的 rdkafka producer 函数调用
     */
    public function rawProducerCall($method, array $args = [])
    {
        return call_user_func_array([$this->producer, $method], $args);
    }

    /**
     * 提供原生的 rdkafka consumer 函数调用
     */
    public function rawConsumerCall($method, array $args = [])
    {
        return call_user_func_array([$this->consumer, $method], $args);
    }
}
