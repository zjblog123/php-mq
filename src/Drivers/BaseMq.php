<?php

namespace General\Mq\Drivers;

class BaseMq
{
    /**
     * 驱动名称
     *
     * @var string
     */
    protected $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }
}
