<?php

namespace General\Mq\Drivers;

use General\Mq\Contract\Queue as MqInterface;
use Illuminate\Contracts\Redis\Factory as Redis;
use Illuminate\Queue\LuaScripts;

class RedisMq extends BaseMq implements MqInterface
{
    /**
     * The Redis factory implementation.
     *
     * @var \Illuminate\Contracts\Redis\Factory
     */
    protected $redis;

    /**
     * The connection name.
     *
     * @var string
     */
    protected $connection;

    /**
     * The name of the default queue.
     *
     * @var string
     */
    protected $default;

    /**
     * The expiration time of a job.
     *
     * @var int|null
     */
    protected $retryAfter = 60;

    /**
     * The maximum number of seconds to block for a job.
     *
     * @var int|null
     */
    protected $blockFor = null;

    /**
     * Create a new Redis queue instance.
     *
     * @param  \Illuminate\Contracts\Redis\Factory  $redis
     * @param  string  $default
     * @param  string|null  $connection
     * @param  int  $retryAfter
     * @param  int|null  $blockFor
     * @return void
     */

    public function __construct(Redis $redis, $default = 'default', $connection = null, $retryAfter = 60, $blockFor = null)
    {
        $this->redis = $redis;
        $this->default = $default;
        $this->blockFor = $blockFor;
        $this->connection = $connection;
        $this->retryAfter = $retryAfter;
    }

    public function push(string $topic, string $message, array $headers = NULL, int $delay = 0)
    {
        return $this->pushRaw($message, $topic);
    }

    public function pull(string $topic)
    {
        // todo
    }

    /**
     * 清空消息
     * @param string $topic
     * @return mixed
     */
    public function flush(string $topic)
    {
        // todo
    }

    /**
     * 消息长度
     * @param int $topic
     * @return mixed
     */
    public function len(int $topic)
    {
        // todo
    }

    public function ack()
    {
        // todo
    }

    public function pushRaw($payload, $queue = null, array $options = [])
    {
        $this->getConnection()->eval(
            LuaScripts::push(), 2, $this->getQueue($queue),
            $this->getQueue($queue).':notify', $payload
        );

        return json_decode($payload, true)['id'] ?? null;
    }

    /**
     * Get the connection for the queue.
     *
     * @return \Illuminate\Redis\Connections\Connection
     */
    protected function getConnection()
    {
        return $this->redis->connection($this->connection);
    }

    /**
     * Get the queue or return the default.
     *
     * @param  string|null  $queue
     * @return string
     */
    public function getQueue($queue)
    {
        return 'queues:'.($queue ?: $this->default);
    }
}
