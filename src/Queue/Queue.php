<?php

namespace General\Mq\Queue;

use General\Mq\Contract\Queue as MqInterface;
use General\Mq\Job\BaseJob;
use General\Mq\Message\Message;
use Exception;

class Queue
{
    /** @var MqInterface $queueDriver 驱动 */
    protected $queueDriver;

    public function setQueueDriver(MqInterface $queueDriver): self
    {
        $this->queueDriver = $queueDriver;
        return $this;
    }

    /**
     * 普通消息
     * @param Message $message
     * @throws Exception
     */
    public function push(Message $message)
    {
        $topic = $message->getTopic();
        if (empty($topic)) {
            throw new Exception('业务类型有误');
        }
        $message->setMessageId(md5(time()));

        $this->queueDriver->push($topic, json_encode($message->toArray()));
    }

    /**
     * 异步任务
     * @param BaseJob $baseJob
     * @param $topic
     */
    public function asyncTask(BaseJob $baseJob, $topic)
    {
        $message = new Message(['topic' => $topic]);

        $message->setMessageId(md5(time()))
            ->setPayload(serialize($baseJob));

        $this->queueDriver->push($message->getTopic(), json_encode($message->toArray()));
    }
}
