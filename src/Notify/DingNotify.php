<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: zcy
 * Date: 2023/2/5
 * Time: 14:07
 */

namespace General\Mq\Notify;


use General\Mq\Contract\Notify;
use General\Mq\Event\EventMessage;
use GuzzleHttp\Client as GuzzleHttpClient;


class DingNotify implements Notify
{

    const BASE_URL = "https://oapi.dingtalk.com";
    const PATH = "/robot/send";

    public function notify(EventMessage $eventMessage)
    {
        $notifyUrl = self::BASE_URL . self::PATH . 'accessToken';
        app(GuzzleHttpClient::class)->postAsync($notifyUrl, [
            'json' => [
                "msgtype" => "text",
                "text" => [
                    "content" => substr('报错信息', 0, 1500) . '...'
                ],
                "at" => [
                    "atMobiles" => [],
                    "isAtAll" => false
                ]
            ]
        ]);
    }
}
