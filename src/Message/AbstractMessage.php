<?php


namespace General\Mq\Message;


use General\Mq\Contract\Message;

abstract class AbstractMessage implements Message
{
    protected $topic;

    protected $messageId;

    protected $payload;

    protected $attempts = 0;

    protected $maxTries = 3;

    protected $lastProcessTime;

    protected $callback;

    protected $extendData = [];

    public function __construct(array $data)
    {
        $this->topic = $data['topic'] ?? '';
        $this->payload = $data['payload'] ?? null;
        $this->attempts = $data['attempts'] ?? 0;
        $this->maxTries = $data['max_tries'] ?? 3;
        $this->callback = $data['callback'] ?? null;
        $this->extendData = $data['extend_data'] ?? [];
    }

    /**
     * 获取主题
     * @return mixed
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * 获取消息ID
     * @return mixed
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * 获取消息实体
     * @return mixed
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * 重试次数
     * @return mixed
     */
    public function getAttempts()
    {
        return $this->attempts;
    }

    /**
     * 最大重试次数
     * @return mixed
     */
    public function getMaxTries()
    {
        return $this->maxTries;
    }

    /**
     * 最后一次处理时间
     * @return mixed
     */
    public function getLastProcessTime()
    {
        return $this->lastProcessTime;
    }

    /**
     * 回调函数
     * @return mixed
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * 扩展的保留数据
     * @return mixed
     */
    public function getExtendData()
    {
        return $this->extendData;
    }

    /**
     * 设置主题
     * @param mixed $topic
     */
    public function setTopic($topic): self
    {
        $this->topic = $topic;
        return $this;
    }

    /**
     * 设置消息实体
     * @param $payload
     * @return $this
     */
    public function setPayload($payload): self
    {
        $this->payload = $payload;
        return $this;
    }


    /**
     * 设置回调函数
     * @param Callable $callback
     * @return $this
     */
    public function setCallback(callable $callback): self
    {
        $this->callback = $callback;
        return $this;
    }

    /**
     * 设置重试次数
     * @param int $attempts
     * @return $this
     */
    public function setAttempts(int $attempts): self
    {
        $this->attempts = $attempts;
        return $this;
    }

    /**
     * 设置最大重试次数
     * @param int $maxTries
     * @return $this
     */
    public function setMaxTries(int $maxTries): self
    {
        $this->maxTries = $maxTries;
        return $this;
    }

    /**
     * 设置随后处理时间
     * @param string $lastProcessTime
     * @return $this
     */
    public function setLastProcessTime(string $lastProcessTime): self
    {
        $this->lastProcessTime = $lastProcessTime;
        return $this;
    }

    /**
     * 设置消息ID
     * @param mixed $messageId
     * @return $this
     */
    public function setMessageId($messageId): self
    {
        $this->messageId = $messageId;
        return $this;
    }
}
