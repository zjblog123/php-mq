<?php

namespace General\Mq\Message;


use Illuminate\Contracts\Support\Arrayable;

class Message extends AbstractMessage implements Arrayable
{
    public function __construct(array $data)
    {
        $data = $this->fill($data);
        parent::__construct($data);
    }

    public function toArray(): array
    {
        return [
            'topic' => $this->topic,
            'message_id' => $this->messageId,
            'payload' => $this->payload,
            'attempts' => $this->attempts,
            'max_tries' => $this->maxTries,
            'last_process_time' => $this->lastProcessTime,
            'extend_data' => $this->extendData,
            'callback' => $this->callback
        ];
    }

    /**
     * 填充message
     * @param array $message
     * @return array
     * @time: 2023/1/17 17:18
     */
    public function fill(array $message): array
    {
        $data = [
            'topic' => $message['topic'] ?? '',
            'message_id' => $message['message_id'] ?? '',
            'payload' => $message['payload'] ?? '',
            'attempts' => $message['attempts'] ?? 0,
            'max_tries' => $message['max_tries'] ?? 3,
            'last_process_time' => $message['last_process_time'] ?? '',
            'extend_data' => $message['extend_data'] ?? [],
            'callback' => $message['callback'] ?? null,
        ];

        return $data;
    }
}
