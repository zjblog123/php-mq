<?php

namespace General\Mq;

use General\Mq\Queue\Queue;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Support\DeferrableProvider;
use General\Mq\Conectors\KafkaConnector;
use General\Mq\Conectors\RedisConnector;

class MqServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerManager();
        $this->registerConnection();
        $this->registerProduct();
    }

    protected function registerManager()
    {
        $this->app->singleton('mq.manger', function() {
            $manager = new MqManager();
            $this->registerConnectors($manager);
            return $manager;
        });
    }

    protected function registerConnection()
    {
        $this->app->singleton('mq.connection', function($app) {
            return $app['mq.manger']->connection();
        });
    }

    public function registerConnectors($manager)
    {
        foreach (['Redis', 'Kafka'] as $connector) {
            $this->{"register{$connector}Connector"}($manager);
        }
    }

    protected function registerRedisConnector($manager)
    {
        $manager->addConnector('redis', function() {
            return new RedisConnector($this->app['redis']);
        });
    }

    protected function registerKafkaConnector($manager)
    {
        $manager->addConnector('kafka', function() {
            return new KafkaConnector();
        });
    }

    protected function registerProduct()
    {
        $this->app->singleton('mq', function($app) {
            return (new Queue())->setQueueDriver($app['mq.connection']);
        });
    }
}
