<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: zcy
 * Date: 2023/1/6
 * Time: 16:44
 */

namespace General\Mq\Job;


use General\Mq\Contract\Job;

class BaseJob implements Job
{

    public $topicType;  // topic类型

    public $data;  //队列参数

    public function __construct(int $topicType, array $data)
    {
        $this->topicType = $topicType;
        $this->data = $data;
    }

    public function execute()
    {
        // TODO: Implement getMessageId() method.
    }


}
