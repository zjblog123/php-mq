<?php

namespace General\Mq\Conectors;

use General\Mq\Contract\Connector as ConnectorInterface;
use General\Mq\Contract\Queue as MqInterface;
use General\Mq\Drivers\KafkaMq;
use InvalidArgumentException;
use RdKafka\Conf as RdKafkaConf;
use RdKafka\KafkaConsumer;
use RdKafka\Producer;

class KafkaConnector implements ConnectorInterface
{
    public function connect(array $config): MqInterface
    {
        if (!isset($config['metadata.broker.list'])) {
            throw new InvalidArgumentException("Kafka config require metadata.broker.list");
        }

        if (!isset($config['producer']) || !isset($config['consumer'])) {
            throw new InvalidArgumentException("Kafka config require producer and consumer");
        }

        $globalConfig = collect($config)->except(['driver', 'producer', 'consumer'])->toArray();

        return new KafkaMq(
            $this->newProducer($globalConfig, $config['producer']),
            $this->newConsume($globalConfig, $config['consumer'])
        );
    }

    /**
     *  构造 RdKafka Producer
     */
    protected function newProducer(array $globalConfig, array $producerConfig)
    {
        $conf = new RdKafkaConf();

        collect($globalConfig)->merge($producerConfig)->each(function ($attrValue, $attrKey) use ($conf) {
            $conf->set((string)$attrKey, $attrValue);
        });

        return new Producer($this->settingGlobalCallback($conf));
    }

    /**
     * 构造 RdKafka Consume
     */
    protected function newConsume(array $globalConfig, array $consumeConfig)
    {
        $conf = new RdKafkaConf();

        collect($globalConfig)->merge($consumeConfig)->each(function ($attrValue, $attrKey) use ($conf) {
            $conf->set((string)$attrKey, $attrValue);
        });

        return new KafkaConsumer($this->settingGlobalCallback($conf));
    }

    /**
     * RdKafka 全局回调设置
     */
    protected function settingGlobalCallback(RdKafkaConf $conf)
    {
        /*
        $conf->setDrMsgCb(function ($kafka, $message) {
            if ($message->err) {
                // message permanently failed to be delivered
                throw new \Exception("message permanently failed to be delivered");
            } else {
                // message successfully delivered
                // throw new \Exception("message successfully delivered");
            }
        });

        // Set error callback
        $conf->setErrorCb(function ($kafka, $err, $reason) {
            throw new \Exception(sprintf("Kafka error: %s (reason: %s)\n", rd_kafka_err2str($err), $reason));
        });
        */

        return $conf;
    }
}
