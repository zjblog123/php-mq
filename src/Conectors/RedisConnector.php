<?php

namespace General\Mq\Conectors;

use General\Mq\Contract\Connector as ConnectorInterface;
use General\Mq\Contract\Queue as MqInterface;
use General\Mq\Drivers\RedisMq;
use Illuminate\Contracts\Redis\Factory as Redis;

class RedisConnector implements ConnectorInterface
{
    protected $redis;

    protected $connection;

    public function __construct(Redis $redis, $connection = null)
    {
        $this->redis = $redis;
        $this->connection = $connection;
    }

    public function connect(array $config): MqInterface
    {
        return new RedisMq(
            $this->redis, $config['queue'],
            $config['connection'] ?? $this->connection,
            $config['retry_after'] ?? 60,
            $config['block_for'] ?? null
        );
    }
}
