<?php

namespace General\Mq\Contract;

interface Message
{
    /**
     * 获取消息ID
     * @return mixed
     */
    public function getMessageId();

    /**
     * 获取消息实体
     * @return mixed
     */
    public function getPayload();

    /**
     * 重试次数
     * @return mixed
     */
    public function getAttempts();

    /**
     * 最大重试次数
     * @return mixed
     */
    public function getMaxTries();

    /**
     * 最后一次处理时间
     * @return mixed
     */
    public function getLastProcessTime();

    /**
     * 回调函数
     * @return mixed
     */
    public function getCallback();

    /**
     * 注册回调函数
     * @param callable $callback
     * @return mixed
     */
    public function setCallback(callable $callback);

    /**
     * 扩展的保留数据
     * @return mixed
     */
    public function getExtendData();
}
