<?php

namespace General\Mq\Contract;

use General\Mq\Exceptions\NoMoreMessageException;

interface Queue
{
    /**
     * 推送消息
     * @param string $topic
     * @param string $message
     * @param array|null $headers
     * @param int $delay
     * @return mixed
     */
    public function push(string $topic, string $message, array $headers = NULL, int $delay = 0);

    /**
     * 拉取消息
     * @param string $topic
     * @return mixed
     * @throws NoMoreMessageException
     */
    public function pull(string $topic);

    /**
     * 清空消息
     * @param string $topic
     * @return mixed
     */
    public function flush(string $topic);

    /**
     * 消息长度
     * @param int $topic
     * @return mixed
     */
    public function len(int $topic);

    /**
     * 消费者 - 消费确认
     */
    public function ack();

    /**
     * 驱动名称
     * @return string
     */
    public function getName(): string;

    /**
     * 设置驱动名称
     * @param string $name
     */
    public function setName(string $name);
}
