<?php

namespace General\Mq\Contract;

interface BusinessHandle
{
    public function execute($content): bool;
}
