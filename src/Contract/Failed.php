<?php

namespace General\Mq\Contract;

use General\Mq\Event\EventMessage;

interface Failed
{
    /**
     * 记录日志
     *
     * @param string $driver 驱动名称
     * @param $topic
     * @param $message
     * @param $exception
     * @param array $extend
     * @author zcy
     */
    public function log(EventMessage $eventMessage);

    /**
     * @return mixed
     */
    public function all();

    /**
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * @param $id
     * @return mixed
     */
    public function forget($id);

    /**
     * @return mixed
     */
    public function flush();
}
