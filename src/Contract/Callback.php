<?php

namespace General\Mq\Contract;

interface Callback
{
    /**
     * 闭包
     * @param $message
     * @return mixed
     */
    public function call($message);
}
