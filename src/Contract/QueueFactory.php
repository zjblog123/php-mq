<?php

namespace General\Mq\Contract;

interface QueueFactory
{
    /**
     * 获取MQ实例
     * @return mixed
     */
    public function guard(string $name);
}
