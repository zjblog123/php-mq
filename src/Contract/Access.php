<?php

namespace General\Mq\Contract;

interface Access
{
    /**
     * 根据code码订阅topic
     * @param $code
     * @return mixed
     */
    public function subscribe($code);

    /**
     * 根据code码取消订阅topic
     * @param $code
     * @return mixed
     */
    public function unsubscribe($code);
}
