<?php

namespace General\Mq\Contract;

interface HandleFactory
{
    /**
     * 获取队列处理器
     * @return mixed
     */
    public function guard();
}
