<?php

namespace General\Mq\Contract;

interface Job
{
    /**
     * 执行异步任务
     * @return mixed
     */
    public function execute();
}
