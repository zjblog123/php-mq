<?php

namespace General\Mq\Contract;

use General\Mq\Contract\Queue as MqInterface;

interface Connector
{
    public function connect(array $config): MqInterface;
}
