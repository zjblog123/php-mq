<?php

namespace General\Mq\Contract;

interface Observer
{
    public function notify($message);
}
