<?php

namespace General\Mq\Contract;

use General\Mq\Event\EventMessage;

interface Notify
{
    /**
     * 通知消息
     *
     * @param EventMessage $eventMessage
     * @return mixed
     * @author zcy
     */
    public function notify(EventMessage $eventMessage);
}
