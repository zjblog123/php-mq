<?php

namespace General\Mq\Resources;

use Illuminate\Config\Repository;

class ConfigBean
{
    /** @var Repository */
    public $config;

    public function __construct()
    {
        $this->config = app('config');
    }

    public function getConfig($driver): array
    {
        return $this->config->get(sprintf('mq.connections.%s', $driver));
    }

    public function getDefaultDriver()
    {
        return $this->config->get('mq.default');
    }

    public function getFailedTable()
    {
        return $this->config->get('mq.failed.table');
    }
}
