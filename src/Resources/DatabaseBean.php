<?php

namespace General\Mq\Resources;

class DatabaseBean
{
    public $db;

    public function __construct()
    {
        $this->db = app('db');
    }

    public function insert($table, $data)
    {
        return $this->db->insert("insert into {$table} (driver, topic, message, exception, failed_at) values (?, ?, ?, ?, ?)", $data);
    }

    public function all($table)
    {
        return $this->db->select("select * from {$table} order by id desc");
    }

    public function find($table, $id)
    {
        $result = $this->db->select("select * from {$table} where id = ? limit 1;", [$id]);
        return array_pop($result);
    }

    public function delete($table, $id)
    {
        return $this->db->statement("delete from {$table} where id = ? limit 1;", [$id]);
    }

    public function truncate($table)
    {
        return $this->db->statement("truncate {$table}");
    }
}
